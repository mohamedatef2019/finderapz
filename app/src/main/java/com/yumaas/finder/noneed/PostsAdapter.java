package com.yumaas.finder.noneed;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.finder.R;
import com.yumaas.finder.base.DefaultRequest;
import com.yumaas.finder.base.UserPreferenceHelper;
import com.yumaas.finder.base.volleyutils.ConnectionHelper;
import com.yumaas.finder.base.volleyutils.ConnectionListener;
import com.yumaas.finder.login.LoginActivity;
import com.yumaas.finder.login.LoginRequest;
import com.yumaas.finder.login.UserResponse;
import com.yumaas.finder.posts.DeletePostRequest;
import com.yumaas.finder.posts.response.PostsItem;

import java.util.ArrayList;
import java.util.List;


public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder> {

    Context context;
    List<PostsItem> postsItems;


    public PostsAdapter(Context context,List<PostsItem>postsItems) {
        this.postsItems=postsItems;
        this.context = context;
    }


    @Override
    public PostsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post, parent, false);
        PostsAdapter.ViewHolder viewHolder = new PostsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PostsAdapter.ViewHolder holder, final int position) {

        holder.name.setText(postsItems.get(position).getUser().getName());

        if(postsItems.get(position).getUserId().contains(""+UserPreferenceHelper.getUserDetails().getId())){

            holder.deleteBtn.setVisibility(View.VISIBLE);
        }else {

            holder.deleteBtn.setVisibility(View.GONE);
        }

        holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new  AlertDialog.Builder(v.getContext());
                dialog.setMessage("Sure you need to delete this item");


                dialog.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deletePost(postsItems.get(position).getId(),position);
                        dialog.cancel();
                        dialog.dismiss();
                    }
                });

                dialog.setPositiveButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        holder.details.setText(
                postsItems.get(position).getPhone()+"\n"
                +postsItems.get(position).getDetails());

        try {
            Picasso.get().load(postsItems.get(position).getImages().get(0).getImage()).into(holder.image);
        }catch (Exception e){
            e.getStackTrace();
        }
        holder.itemView.setOnClickListener(view -> {

          //  FragmentHelper.replaceFragment(view.getContext(), new ChildDetaillsFragment(), "CompanyDetailsFragment");


        });


    }


    public void deletePost(int id,int position){

        DeletePostRequest deletePostRequest = new DeletePostRequest("delete_post");
        deletePostRequest.setPostId(id+"");

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                Toast.makeText(context, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                postsItems.remove(position);
                notifyDataSetChanged();
             }

            @Override
            public void onRequestError(Object error) {

            }
        }).requestJsonObject(deletePostRequest, DefaultRequest.class);
    }


    @Override
    public int getItemCount() {
        return postsItems==null?0:postsItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name,details;
        Button deleteBtn;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            this.name=itemView.findViewById(R.id.name);
            this.details=itemView.findViewById(R.id.details);
            this.image=itemView.findViewById(R.id.image);
            this.deleteBtn=itemView.findViewById(R.id.btn_delete);

        }
    }
}
