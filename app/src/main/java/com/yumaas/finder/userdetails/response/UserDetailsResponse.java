package com.yumaas.finder.userdetails.response;

import com.google.gson.annotations.SerializedName;
import com.yumaas.finder.home.response.ChildsItem;

public class UserDetailsResponse{

	@SerializedName("state")
	private int state;

	@SerializedName("child_details")
	private ChildsItem childDetails;

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setChildDetails(ChildsItem childDetails){
		this.childDetails = childDetails;
	}

	public ChildsItem getChildDetails(){
		return childDetails;
	}
}