package com.yumaas.finder.userdetails;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.yumaas.finder.R;
import com.yumaas.finder.base.UserPreferenceHelper;
import com.yumaas.finder.base.volleyutils.ConnectionHelper;
import com.yumaas.finder.base.volleyutils.ConnectionListener;
import com.yumaas.finder.home.UsersRequest;
import com.yumaas.finder.login.LoginActivity;
import com.yumaas.finder.login.LoginRequest;
import com.yumaas.finder.login.UserResponse;
import com.yumaas.finder.noneed.MainActivity;
import com.yumaas.finder.noneed.SliderAdapterBarber;
import com.yumaas.finder.userdetails.response.UserDetailsResponse;


public class UserDetailsFragment extends Fragment {

    View rootView;
    SliderView sliderView;
    TextView name,phone,email,address;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fagment_child_details, container, false);
        name = rootView.findViewById(R.id.name);
        phone = rootView.findViewById(R.id.phone);
        email = rootView.findViewById(R.id.email);
        address = rootView.findViewById(R.id.address);

        getUserDetails();


        return rootView;
    }

    private void setSliderView(String[] images){

        sliderView =  rootView.findViewById(R.id.imageSlider);
        sliderView.setSliderAdapter(new SliderAdapterBarber(getActivity(),images));
        sliderView.startAutoCycle();
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setIndicatorAnimation(IndicatorAnimationType.THIN_WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.FANTRANSFORMATION);
        sliderView.setScrollTimeInSec(4);
    }

    public void getUserDetails(){

        UsersRequest loginRequest = new UsersRequest("child_details");
        loginRequest.setCode(getArguments().getString("code"));


        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                UserDetailsResponse userResponse = (UserDetailsResponse)response;
                if (userResponse.getState()==101){
                    String[] images = new String[1];
                    images[0]=userResponse.getChildDetails().getImage();
                    setSliderView(images);
                    name.setText(userResponse.getChildDetails().getName());
                    phone.setText(userResponse.getChildDetails().getPhone());
                    address.setText(userResponse.getChildDetails().getAddress());
                    email.setText(userResponse.getChildDetails().getEmail());
                }

            }

            @Override
            public void onRequestError(Object error) {

            }
        }).requestJsonObject(loginRequest, UserDetailsResponse.class);
    }
}