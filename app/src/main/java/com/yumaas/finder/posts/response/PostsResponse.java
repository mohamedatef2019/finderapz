package com.yumaas.finder.posts.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class PostsResponse{

	@SerializedName("state")
	private int state;

	@SerializedName("posts")
	private List<PostsItem> posts;

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setPosts(List<PostsItem> posts){
		this.posts = posts;
	}

	public List<PostsItem> getPosts(){
		return posts;
	}
}