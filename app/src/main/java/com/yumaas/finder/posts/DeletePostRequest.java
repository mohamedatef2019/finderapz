package com.yumaas.finder.posts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.yumaas.finder.base.DefaultRequest;


public class DeletePostRequest extends DefaultRequest {


    @Expose
    @SerializedName("id")
    private String postId;




    public DeletePostRequest(String method) {
        super(method);
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostId() {
        return postId;
    }
}
